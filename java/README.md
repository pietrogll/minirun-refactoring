# Checkout.java

## Requisitos
Para poder trabajar sobre este ejercicio te basta con tener instalado `docker`. Si prefieres la instalación en local, necesitarás `java 10`.

## Preparación

### Ejecutar tests
#####./mvnw test

### Generar el .jar además de generar y añadir la imagen de docker
#####./mvnw package
 
##Ejecución
Podemos ejecutar el programa de command line que hemos preparado para hacer pruebas manuales del Checkout. 
Puedes introducir una cadena con los articulos y te devuelve el precio final.

### Ejecución en Docker
##### docker/run


### Ejecución en entorno local
##### java -jar target/refactoring-0.0.1.jar


