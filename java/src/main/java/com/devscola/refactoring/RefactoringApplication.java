package com.devscola.refactoring;

import java.util.Scanner;

//@SpringBootApplication
public class RefactoringApplication {

    public static void main(String[] args) {
        // SpringApplication.run(RefactoringApplication.class, args);

        System.out.println("Put items to buy: ");
        var checkout = new Checkout(new PricingRules());

        var scanner = new Scanner(System.in);
        var items = scanner.nextLine();
        checkout.scan(items);
        System.out.println(String.format("Total price: %s", checkout.total()));
    }

}
